package main

import data.functions

## Fail if one resource is going to be deleted
deny[msg] {
    avoid := "delete"

    item = input.resource_changes[_]

    some i
        item.change.actions[i] == avoid

        msg = sprintf(
            "The resource '%v' from type '%v' is going to be destroyed or re-created. Exiting...",
            [item.name, item.type]
        )
}

## Fail if one variable has not a description assigned.
deny[msg] {
	item = input.configuration.root_module.variables[key]

	functions.get_desc(item) == ""

	msg := sprintf("The variable '%v' doesn't have any description", 
	    [key]
    )
}

## Check if any modified resource is going to have the correct tags or not
## Based on https://github.com/Scalr/sample-tf-opa-policies/blob/master/management/resource_tags.rego
deny[msg] {
    item := input.resource_changes[_]
    action := item.change.actions[count(item.change.actions) - 1]
    functions.array_contains(["create", "update"], action)

    tags := functions.get_tags(item)
    existing_tags := [ key | tags[key] ]
    required_tag := functions.required_tags[_]
    
    not functions.array_contains(existing_tags, required_tag)

    msg := sprintf(
        "The resource '%v from type '%v' doesn't have the required %q tag/label. Exiting...",
        [item.name, item.address, required_tag]
    )
}
