package functions

## Testing variables
## Define the minimum required tags for management
required_tags = ["environment", "management-tool", "app"]

## array_contains function checks if there is one element inside an array
array_contains(arr, elem) = true {
  arr[_] = elem
} 
else = false { 
  true 
}

## get_basename function check if the provider is from one developer or not
get_basename(path) = basename{
    arr := split(path, "/")
    basename:= arr[count(arr)-1]
}

## get_tags function retrieves all thre tags/labels in function of one provider. In 
## GCP they are called labels, but in other providers are called tags.
get_tags(resource) = labels {
    provider_name := get_basename(resource.provider_name)
    "google" == provider_name
    labels := resource.change.after.labels
} else = tags {
    tags := resource.change.after.tags
}

## get_desc function checks if the variables has a description defined
get_desc(var) = desc {
  desc := var.description
} else = no_desc {
  no_desc := ""
}
