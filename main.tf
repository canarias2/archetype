# Resources related to Cloud Storage services
resource "google_storage_bucket" "dummy" {
  name          = "${var.workload_name}-${var.gcp_default_region}-${var.gcp_environment}"
  location      = var.gcp_default_region
  force_destroy = false

  uniform_bucket_level_access = true

  labels = local.labels
}
