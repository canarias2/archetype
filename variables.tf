# General variables related to GCP
variable "gcp_sudo_account" {
  type        = string
  description = "GCP SA to impersonate to deploy any changes in Atlantis infrastructure"
}

variable "gcp_environment" {
  type        = string
  description = "Environment to deploy the workload"
}

variable "gcp_default_project" {
  type        = string
  description = "Default project of the whole environment"
}

variable "gcp_default_region" {
  type        = string
  default     = "europe-west1"
  description = "Default region of the whole environment"
}

# Variables related to Atlantis deployment
variable "workload_name" {
  type        = string
  default     = "dummy"
  description = "Default name for the workload deployed"
}
