locals {
  labels = {
    app             = var.workload_name
    environment     = var.gcp_environment
    management-tool = "terraform"
  }
}
