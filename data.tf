# Data sources for managing projects
data "google_project" "project" {
  project_id = var.gcp_default_project
}
