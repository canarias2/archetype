# Dummy
This repository will host all the dummy need to test the Atlantis service.


## Initialization
To start this project and connect to the remote state, you must have permissions to access the remote state bucket and to modify the resources in the Google project. The safest way to do it is with [impersonation](https://cloud.google.com/iam/docs/impersonating-service-accounts).

The basic GCP account must have the role _Service Account Token Creator_ and IAM and Cloud Resource Manager APIs must be previously enabled.
You should define in your init-tfvars and apply-tfvars one variable called _impersonate\_service\_account_ with the GCP account that should be impersonated

After that, you can connect to the remote state, use the following commnad:

```hcl
terraform init -backend-config=init-tfvars/dev.tfvars && terraform validate
```

## Requirements
This repo relies on SOPS and Google KMS to work, remember to replace your own prd.tfvars.enc and .sops.yaml file to have it working.
