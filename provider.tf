provider "google" {
  project = var.gcp_default_project
  region  = var.gcp_default_region

  impersonate_service_account = var.gcp_sudo_account
}
